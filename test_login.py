import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import time

class PythonOrgSearch(unittest.TestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument("--start-maximized")
		#self.driver = webdriver.Chrome('C:/chromedriver_win32/chromedriver.exe',options=chrome_options)
		self.driver = webdriver.Remote(command_executor='http://jenkins.floraqueen.net:4444/wd/hub',options=chrome_options,desired_capabilities=DesiredCapabilities.CHROME)
		#self.driver = webdriver.Chrome('/usr/bin/chromedriver',options=chrome_options)
		#self.driver = webdriver.Firefox()

	def test_search_in_python_org(self):
		driver = self.driver
		driver.implicitly_wait(30)
		driver.get('https://accounts.google.com/signin/v2/identifier?service=mail&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&hl=es&flowName=GlifWebSignIn&flowEntry=ServiceLogin#__utma=29003808.18383240.1541166934.1541166934.1541166934.1&__utmb=29003808.0.10.1541166934&__utmc=29003808&__utmx=-&__utmz=29003808.1541166934.1.1.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided)&__utmv=-&__utmk=56058088');


		time.sleep(1)
		gmail= driver.find_element_by_name('identifier')
		gmail.send_keys('test.floraqueen.test@gmail.com')
		driver.find_element_by_xpath(".//*[@class='CwaK9']").click()

		time.sleep(4)
		gmail= driver.find_element_by_name('password')
		gmail.send_keys('F!oraqueen1990')
		time.sleep(8)
		driver.find_element_by_xpath(".//*[@class='CwaK9']").click()

		time.sleep(4)
		driver.find_element_by_xpath(".//div[@class='yW']").click()
		time.sleep(4)
		password = driver.find_element_by_css_selector('div:nth-child(1) > center > div > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td > table > tbody > tr > td > table > tbody > tr:nth-child(2) > td:nth-child(2)').text
		time.sleep(4)
		driver.get('https://www.floraqueen.com');
		#self.assertIn("Python", driver.title)
		#elem = driver.find_element_by_name("q")
		#elem.send_keys("pycon")
		#elem.send_keys(Keys.RETURN)
		#assert "No results found." not in driver.page_source
		print (password)
		try:
			driver.find_element_by_css_selector("button.close").click()
		except Exception as e:
			pass
		menu = driver.find_element_by_css_selector("i.fq-user_full")
		actions = ActionChains(driver)
		actions.move_to_element(menu)
		actions.perform()
		time.sleep(1)
		#hidden_submenu = driver.find_element_by_css_selector(".nav #submenu1")
		driver.find_element_by_name('user_email').send_keys('test.floraqueen.test@gmail.com') 
		driver.find_element_by_name('user_password').send_keys(password)
		driver.find_element_by_css_selector('button.button-login').click()
		time.sleep(5)
		header = driver.find_element_by_css_selector('header.l-header')
		assert "Hi Test!" in header.text

		time.sleep(5)


	def tearDown(self):
		self.driver.quit()

if __name__ == "__main__":
	unittest.main()









#driver.quit()