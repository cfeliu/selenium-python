import unittest
import time
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
from datetime import datetime,timedelta
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--start-maximized")
        #self.driver = webdriver.Chrome('C:/chromedriver_win32/chromedriver.exe',options=chrome_options)
        self.driver = webdriver.Remote(command_executor='http://jenkins.floraqueen.net:4444/wd/hub',options=chrome_options,desired_capabilities=DesiredCapabilities.CHROME)
        #self.driver = webdriver.Firefox()

    def test_search_in_python_org(self):
        driver = self.driver
        #driver.get("http://www.python.org")
        #self.assertIn("Python", driver.title)
        #elem = driver.find_element_by_name("q")
        #elem.send_keys("pycon")
        #elem.send_keys(Keys.RETURN)
        #assert "No results found." not in driver.page_source
        driver.implicitly_wait(30)
        url = 'https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fflowers-stage-en.floraqueen.net%2F&tab=mobile'
        driver.get(url);
        self.assertLessEqual(50,int(driver.find_element_by_css_selector('div.lh-gauge__percentage').text))
        #time.sleep(5)
        url = 'https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fflowers-stage-es.floraqueen.net%2F&tab=mobile'
        driver.get(url);
        self.assertLessEqual(50,int(driver.find_element_by_css_selector('div.lh-gauge__percentage').text))
        #time.sleep(5)
        url = 'https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fflowers-stage-it.floraqueen.net%2F&tab=mobile'
        driver.get(url);
        self.assertLessEqual(50,int(driver.find_element_by_css_selector('div.lh-gauge__percentage').text))
        #time.sleep(5)
        url = 'https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fflowers-stage-de.floraqueen.net%2F&tab=mobile'
        driver.get(url);
        self.assertLessEqual(50,int(driver.find_element_by_css_selector('div.lh-gauge__percentage').text))
        #time.sleep(5)
        url = 'https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fflowers-stage-fr.floraqueen.net%2F&tab=mobile'
        driver.get(url);
        self.assertLessEqual(50,int(driver.find_element_by_css_selector('div.lh-gauge__percentage').text))
        #time.sleep(5)
        url = 'https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fflowers-stage-pl.floraqueen.net%2F&tab=mobile'
        driver.get(url);
        self.assertLessEqual(50,int(driver.find_element_by_css_selector('div.lh-gauge__percentage').text))
        #time.sleep(5)
        url = 'https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fflowers-stage-en.floraqueen.net%2F&tab=desktop'
        driver.get(url);
        self.assertLessEqual(50,int(driver.find_elements_by_css_selector('div.lh-gauge__percentage')[1].text))
        #time.sleep(5)
        url = 'https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fflowers-stage-es.floraqueen.net%2F&tab=desktop'
        driver.get(url);
        self.assertLessEqual(50,int(driver.find_elements_by_css_selector('div.lh-gauge__percentage')[1].text))
        #time.sleep(5)
        url = 'https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fflowers-stage-it.floraqueen.net%2F&tab=desktop'
        driver.get(url);
        self.assertLessEqual(50,int(driver.find_elements_by_css_selector('div.lh-gauge__percentage')[1].text))
        #time.sleep(5)
        url = 'https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fflowers-stage-de.floraqueen.net%2F&tab=desktop'
        driver.get(url);
        self.assertLessEqual(50,int(driver.find_elements_by_css_selector('div.lh-gauge__percentage')[1].text))
        #time.sleep(5)
        url = 'https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fflowers-stage-fr.floraqueen.net%2F&tab=desktop'
        driver.get(url);
        self.assertLessEqual(50,int(driver.find_elements_by_css_selector('div.lh-gauge__percentage')[1].text))
        #time.sleep(5)
        url = 'https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fflowers-stage-pl.floraqueen.net%2F&tab=desktop'
        driver.get(url);
        self.assertLessEqual(50,int(driver.find_elements_by_css_selector('div.lh-gauge__percentage')[1].text))
        #time.sleep(5)


    def tearDown(self):
        self.driver.quit()

if __name__ == "__main__":
    unittest.main()



