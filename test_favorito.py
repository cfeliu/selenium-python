#!/usr/bin/env python
# -*- coding: utf-8 -*-
import unittest
import time
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
from datetime import datetime,timedelta
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.chrome.options import Options

class PythonOrgSearch(unittest.TestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument("--start-maximized")
		#self.driver = webdriver.Chrome('C:/chromedriver_win32/chromedriver.exe',options=chrome_options)
		self.driver = webdriver.Remote(command_executor='http://jenkins.floraqueen.net:4444/wd/hub',options=chrome_options,desired_capabilities=DesiredCapabilities.CHROME)

	def test_search_in_python_org(self):
		driver = self.driver
		driver.get('https://flowers-stage-en.floraqueen.net');

		time.sleep(2)

		try:
			driver.find_element_by_css_selector("button.close").click()
		except Exception as e:
			pass	


		time.sleep(1)
		article_list = driver.find_elements_by_xpath("//a[@data-layer-label='feliz cumpleaños | flowers-es']")
		article_list[0].click()


		time.sleep(1)
		driver.find_element_by_xpath(".//a[@class='add-to-favorites']").click()

		header = driver.find_element_by_css_selector('header.l-header')
		assert "1" in header.text
		time.sleep(2)

		driver.find_element_by_xpath(".//li[@class='toolbar_items']").click()

		header = driver.find_element_by_css_selector('article.catalog-product')
		assert "Pink Bubbles: Roses and Alstroemerias" in header.text
		time.sleep(2)

		time.sleep(1)
		driver.find_element_by_xpath(".//a[@class='add-to-favorites checked']").click()

		header = driver.find_element_by_id('no-favorites')
		assert "Add favourites!" in header.text
		time.sleep(2)

		header = driver.find_element_by_css_selector('a.view-favorites')
		assert "" in header.text
		time.sleep(2)


	def tearDown(self):
		self.driver.quit()

if __name__ == "__main__":
	unittest.main()



