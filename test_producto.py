#!/usr/bin/env python
# -*- coding: utf-8 -*-
import unittest
import time
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
from datetime import datetime,timedelta
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.chrome.options import Options

class PythonOrgSearch(unittest.TestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument("--start-maximized")

		#self.driver = webdriver.Chrome('C:/chromedriver_win32/chromedriver.exe',options=chrome_options)
		self.driver = webdriver.Remote(command_executor='http://jenkins.floraqueen.net:4444/wd/hub',options=chrome_options,desired_capabilities=DesiredCapabilities.CHROME)

	def test_search_in_python_org(self):
		driver = self.driver
		driver.get('https://flowers-stage-en.floraqueen.net');

		time.sleep(2)

		try:
			driver.find_element_by_css_selector("button.close").click()
		except Exception as e:
			pass	


		driver.implicitly_wait(30)
		#fecha de hoy +1
		now = datetime.now()
		dias = timedelta(days=1)
		diamas = now+dias
		dia_actual = str(str(diamas.day)+'/'+str(diamas.month)+'/'+str(diamas.year))

		time.sleep(1)
		article_list = driver.find_elements_by_xpath("//a[@data-layer-label='feliz cumpleaños | flowers-es']")
		article_list[0].click()

		article_list = driver.find_elements_by_xpath("//article")

		if driver.find_element_by_xpath("//article[@data-sku='FQ1223']"):
			article_list[3].click()


		select = Select(driver.find_element_by_id('delivery-country-select'))
		select.select_by_visible_text('Spain Peninsula')

		time.sleep(2)
		calendar = driver.find_element_by_xpath(".//*[@id='calendar-button-date']").click()
		time.sleep(2)
		# dias_disponibles = driver.find_elements_by_css_selector('div.open-day')
		driver.find_element_by_xpath("//div[@data-locale='"+dia_actual+"']").click()

		imagen = driver.find_element_by_tag_name("img")

		time.sleep(1)
		#selecciona el jarron
		driver.find_element_by_css_selector("label.vase-content ").click()


		#click al boton continuar
		driver.find_element_by_xpath(".//*[@class='btn btn-lg btn-submit btn-submit-product']").click()

		driver.find_element_by_xpath(".//*[@class='button button-orange dblock']").click()

		time.sleep(1)
		driver.find_element_by_xpath(".//div[@class='upsell-addon']").click()


		try:
			
			driver.find_element_by_xpath(".//button[@class='submit-addon-btn button button-orange']").click()
		except Exception as e:
			pass

		

		time.sleep(2)

	def tearDown(self):
		self.driver.quit()

if __name__ == "__main__":
	unittest.main()




