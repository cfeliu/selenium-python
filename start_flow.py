import time
import unittest
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
from datetime import datetime,timedelta
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.chrome.options import Options


class PythonOrgSearch(unittest.TestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument("--start-maximized")
		#self.driver = webdriver.Chrome('C:/chromedriver_win32/chromedriver.exe',options=chrome_options)
		self.driver = webdriver.Remote(command_executor='http://jenkins.floraqueen.net:4444/wd/hub',options=chrome_options,desired_capabilities=DesiredCapabilities.CHROME)
		#self.driver = webdriver.Chrome('/usr/bin/chromedriver',options=chrome_options)
		#self.driver = webdriver.Firefox()

	def test_search_in_python_org(self):
		driver = self.driver

		#fecha de hoy +1
		now = datetime.now()
		dias = timedelta(days=1)
		diamas = now+dias

		#dia_actual = str(str(diamas.day)+'/0'+str(diamas.month)+'/'+str(diamas.year))
		dia_actual = str(str(diamas.year)+'-0'+str(diamas.month)+'-0'+str(diamas.day))
		driver.get('https://www.floraqueen.com');


		try:
			driver.find_element_by_css_selector("button.close").click()
		except Exception as e:
			pass	

		#Seleccionar Spain Peninsula como Delivery Country
		select = Select(driver.find_element_by_id('startflow-delivery-country'))
		select.select_by_visible_text('Spain Peninsula')


		time.sleep(1)
		#Abrir el calendario
		calendar = driver.find_element_by_xpath(".//*[@id='startflow-calendar']").click()
		time.sleep(1)
		driver.find_element_by_xpath("//div[@data-date='"+dia_actual+"']").click()


		#Seleccionar type ocassion
		select = Select(driver.find_element_by_id('startflow-occasion'))
		select.select_by_visible_text('Anniversary')


		#Seleccionar type of flower
		select = Select(driver.find_element_by_id('startflow-flower-type'))
		select.select_by_visible_text('Carnations')
		time.sleep(1)

		#click al boton continuar
		driver.find_element_by_xpath(".//*[@id='btn-search']").click()

		time.sleep(1)

	def tearDown(self):
			self.driver.quit()

if __name__ == "__main__":
	unittest.main()