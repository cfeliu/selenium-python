import time
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
from datetime import datetime,timedelta
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.chrome.options import Options

chrome_options = Options()
chrome_options.add_argument("--start-maximized")
driver = webdriver.Remote(command_executor='http://jenkins.floraqueen.net:4444/wd/hub',options=chrome_options,desired_capabilities=DesiredCapabilities.CHROME)
url = 'https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fflowers-stage-en.floraqueen.net%2F&tab=mobile'
driver.get(url);
time.sleep(5)
url = 'https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fflowers-stage-es.floraqueen.net%2F&tab=mobile'
driver.get(url);
time.sleep(5)
url = 'https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fflowers-stage-it.floraqueen.net%2F&tab=mobile'
driver.get(url);
time.sleep(5)
url = 'https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fflowers-stage-de.floraqueen.net%2F&tab=mobile'
driver.get(url);
time.sleep(5)
url = 'https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fflowers-stage-fr.floraqueen.net%2F&tab=mobile'
driver.get(url);
time.sleep(5)
url = 'https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fflowers-stage-pl.floraqueen.net%2F&tab=mobile'
driver.get(url);
time.sleep(5)
url = 'https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fflowers-stage-en.floraqueen.net%2F&tab=desktop'
driver.get(url);
time.sleep(5)
url = 'https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fflowers-stage-es.floraqueen.net%2F&tab=desktop'
driver.get(url);
time.sleep(5)
url = 'https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fflowers-stage-it.floraqueen.net%2F&tab=desktop'
driver.get(url);
time.sleep(5)
url = 'https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fflowers-stage-de.floraqueen.net%2F&tab=desktop'
driver.get(url);
time.sleep(5)
url = 'https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fflowers-stage-fr.floraqueen.net%2F&tab=desktop'
driver.get(url);
time.sleep(5)
url = 'https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fflowers-stage-pl.floraqueen.net%2F&tab=desktop'
driver.get(url);
driver.quit()

