import unittest
import time
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
from datetime import datetime,timedelta
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.chrome.options import Options

class PythonOrgSearch(unittest.TestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument("--start-maximized")

		#self.driver = webdriver.Chrome('C:/chromedriver_win32/chromedriver.exe',options=chrome_options)
		self.driver = webdriver.Remote(command_executor='http://jenkins.floraqueen.net:4444/wd/hub',options=chrome_options,desired_capabilities=DesiredCapabilities.CHROME)

	def test_search_in_python_org(self):
		driver = self.driver
		driver.get('https://www.florachic.com/');

		time.sleep(1)

		#fecha de hoy +1
		now = datetime.now()
		dias = timedelta(days=3)
		diamas = now+dias
		dia_actual = str(str(diamas.day)+'/'+str(diamas.month)+'/'+str(diamas.year))

		driver.find_element_by_xpath("//li[@data-sku='FC10277']").click()


		time.sleep(1)
		calendar = driver.find_element_by_xpath(".//li[@class='like-button calendarModal']").click()
		time.sleep(2)
		driver.find_element_by_xpath("//div[@data-locale='"+dia_actual+"']").click()


		#selecciona el jarron
		time.sleep(1)
		#selecciona el jarron
		driver.find_elements_by_css_selector("li.display-cell")[1].click()

		#click al boton enviar
		driver.find_element_by_xpath(".//button[@class='send-to-button btn ladda-button btn-custom']").click()
		time.sleep(1)

		#llenar formulario
		name = driver.find_element_by_name('delivery_name')
		name.send_keys('Test Nombre')

		last_name = driver.find_element_by_name('delivery_surname')
		last_name.send_keys('Test apellido')

		addres = driver.find_element_by_name('delivery_address_street')
		addres.send_keys('Carrer de Mallorca')

		number = driver.find_element_by_name('delivery_address_number')
		number.send_keys('454')

		# apt = driver.find_element_by_name('delivery_address_apt')
		# apt.send_keys('Test apt')

		zipcode = driver.find_element_by_name('delivery_zipcode')
		zipcode.send_keys('08013')

		city = driver.find_element_by_name('delivery_city')
		city.send_keys('Barcelona')

		phone = driver.find_element_by_name('delivery_phone')
		phone.send_keys('123456789')

		card = driver.find_element_by_name('card_message')
		card.send_keys('Test mensaje')

		#selecciona la primera tarjeta
		time.sleep(1)
		driver.find_element_by_css_selector("#send-to > div.container > div > div > fieldset.form-horizontal.white-bg.padding-top-reduced > div > div:nth-child(4) > div > ul > li:nth-child(1)").click()

		#click boton continuar
		# time.sleep(1)
		driver.find_element_by_id('delivery-data-button').click()

		#selecciona primer producto
		time.sleep(1)
		driver.find_element_by_css_selector("#addons > div.container.hidden-xs > div:nth-child(2) > ul > li.col-sm-2.width-variation.variation-standard").click()
		
		time.sleep(1)

		#click al boton seleccionar
		driver.find_element_by_xpath(".//button[@class='modal-button-select btn btn-block btn-custom']").click()
		time.sleep(1)

		#click al boton seleccionar
		driver.find_element_by_xpath(".//*[@class='finish-button btn ladda-button btn-custom']").click()

		time.sleep(1)

		#llenar formulario

		select = Select(driver.find_element_by_id('sel_entidad'))
		select.select_by_visible_text('Sr.')

		name2 = driver.find_element_by_name('customer_name')
		name2.send_keys('Test Nombre')

		last_name2 = driver.find_element_by_name('customer_surname')
		last_name2.send_keys('Test apellido')

		addres2 = driver.find_element_by_name('customer_address')
		addres2.send_keys('Carrer de Mallorca')

		zipcode2 = driver.find_element_by_name('customer_zipcode')
		zipcode2.send_keys('08013')

		poblacion = driver.find_element_by_name('customer_city')
		poblacion.send_keys('Barcelona ')

		email = driver.find_element_by_name('customer_email')
		email.send_keys('prueba@hotmail.com')

		phone = driver.find_element_by_name('customer_phone')
		phone.send_keys('123456789')


		#selecciona el metodo de pago
		time.sleep(1)
		#selecciona el jarron
		driver.find_element_by_css_selector("#payment-form > div.container > div > div.col-md-8.col-md-push-4 > fieldset:nth-child(3) > div:nth-child(2) > div:nth-child(2) > div > div > label > input[type='radio']").click()

		time.sleep(1)
		driver.find_element_by_xpath("//button[@id='btn-finish']").click()
		time.sleep(3)

		self.assertIn("PayPal", driver.title)
		time.sleep(2)


	def tearDown(self):
		self.driver.quit()

if __name__ == "__main__":
	unittest.main()






