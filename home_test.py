#!/usr/bin/env python
# -*- coding: utf-8 -*-
import unittest
import time
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
from datetime import datetime,timedelta
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.chrome.options import Options

class PythonOrgSearch(unittest.TestCase):

        def setUp(self):
                chrome_options = Options()
                chrome_options.add_argument("--start-maximized")

                #self.driver = webdriver.Chrome('C:/chromedriver_win32/chromedriver.exe',options=chrome_options)
                #self.driver = webdriver.Chrome('/usr/bin/chromedriver',options=chrome_options)
                self.driver = webdriver.Remote(command_executor='http://jenkins.floraqueen.net:4444/wd/hub',options=chrome_options,desired_capabilities=DesiredCapabilities.CHROME)

        def test_search_in_python_org(self):
                driver = self.driver
                driver.get('https://floraqueen.es');

                time.sleep(2)

                try:
                        driver.find_element_by_css_selector("button.close").click()
                except Exception as e:
                        pass


                #fecha de hoy +1
                now = datetime.now()
                dias = timedelta(days=1)
                diamas = now+dias
                #dia_actual = str(str(diamas.day)+'/0'+str(diamas.month)+'/'+str(diamas.year))
                dia_actual = str(str(diamas.year)+'-0'+str(diamas.month)+'-0'+str(diamas.day))

                #Selecciona la fecha
                calendar = driver.find_element_by_id("startflow-calendar").click()
                time.sleep(1)
                driver.find_element_by_xpath("//div[@data-date='"+dia_actual+"']").click()

                time.sleep(1)
                #Seleccionar la ocacion
                select = Select(driver.find_element_by_id('startflow-occasion'))
                select.select_by_visible_text('Cumpleaños')

                time.sleep(1)
                #Seleccionar el tipo de flores
                select = Select(driver.find_element_by_id('startflow-flower-type'))
                select.select_by_visible_text('Rosas')

                time.sleep(1)
                #click al boton continuar
                driver.find_element_by_xpath(".//*[@id='btn-search']").click()


                header = driver.find_element_by_css_selector('article.product')
                assert "Amor infinito: 12 Rosas Rojas" in header.text
                time.sleep(1)


        def tearDown(self):
                self.driver.quit()

if __name__ == "__main__":
        unittest.main()
