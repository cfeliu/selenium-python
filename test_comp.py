#!/usr/bin/env python
# -*- coding: utf-8 -*-
import unittest
import time
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
from datetime import datetime,timedelta
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.chrome.options import Options

class PythonOrgSearch(unittest.TestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument("--start-maximized")

		#self.driver = webdriver.Chrome('C:/chromedriver_win32/chromedriver.exe',options=chrome_options)
		self.driver = webdriver.Remote(command_executor='http://jenkins.floraqueen.net:4444/wd/hub',options=chrome_options,desired_capabilities=DesiredCapabilities.CHROME)
                #self.driver = webdriver.Chrome('/usr/bin/chromedriver',options=chrome_options)

	def test_search_in_python_org(self):
		driver = self.driver
		driver.get('https://floraqueen.es');

		time.sleep(2)

		try:
			driver.find_element_by_css_selector("button.close").click()
		except Exception as e:
			pass	


		#fecha de hoy +1
                now = datetime.now()
                dias = timedelta(days=1)
                diamas = now+dias
                #dia_actual = ' '+str(str(diamas.day)+'/0'+str(diamas.month)+'/'+str(diamas.year))
                dia_actual = str(str(diamas.year)+'-0'+str(diamas.month)+'-0'+str(diamas.day))

                #Selecciona la fecha
                calendar = driver.find_element_by_id("startflow-calendar").click()
                time.sleep(1)
                driver.find_element_by_xpath("//div[@data-date='"+dia_actual+"']").click()

                time.sleep(1)
                #Seleccionar la ocacion
                select = Select(driver.find_element_by_id('startflow-occasion'))
                select.select_by_visible_text('Aniversario')

                time.sleep(1)
                #Seleccionar el tipo de flores
                select = Select(driver.find_element_by_id('startflow-flower-type'))
                select.select_by_visible_text('Rosas')

                time.sleep(1)
                #click al boton continuar
                driver.find_element_by_xpath(".//*[@id='btn-search']").click()


		header = driver.find_element_by_css_selector('article.product')
		assert "Amor infinito: 12 Rosas Rojas" in header.text
		time.sleep(2)


		article_list = driver.find_elements_by_xpath("//article")

		if driver.find_element_by_xpath("//article[@data-sku='FQ108']"):
			article_list[3].click()


		#selecciona el jarron
		time.sleep(1)
		#selecciona el jarron
		driver.find_element_by_css_selector("label.vase-content ").click()


		#click al boton continuar
		driver.find_element_by_xpath(".//*[@class='btn btn-lg btn-submit btn-submit-product']").click()
		time.sleep(1)

		#selecciona el tarjeta
		driver.find_element_by_xpath("//div[@data-itemtype='cards']").click()


		#escribir mensaje
		caja_texto = driver.find_element_by_name('card_message')
		caja_texto.send_keys('Automated Test Message')


		#click boton continuar
		driver.find_element_by_xpath(".//*[@class='button button-orange dblock']").click()
		time.sleep(2)

		try:
			
			driver.find_element_by_xpath(".//button[@class='submit-addon-btn button button-orange']").click()
		except Exception as e:
			pass	


		time.sleep(2)
		#llenar formulario
		full_name = driver.find_element_by_name('delivery_fullname')
		full_name.send_keys('Test Nombre')

		addres = driver.find_element_by_name('delivery_address_street')
		addres.send_keys('Avinguda Diagonal')

		street_no = driver.find_element_by_name('delivery_address_number')
		street_no.send_keys('124')

		recipient_city = driver.find_element_by_name('delivery_city')
		recipient_city.send_keys('Test Barcelona')


		post_code = driver.find_element_by_name('delivery_zipcode')
		post_code.send_keys('08006')

		phone = driver.find_element_by_name('delivery_phone')
		phone.send_keys('123456789')

		name_surname = driver.find_element_by_name('customer_fullname')
		name_surname.send_keys('Test Floraqueen')

		email = driver.find_element_by_name('customer_email')
		email.send_keys('test@floraqueen.com')

		city = driver.find_element_by_name('customer_city')
		city.send_keys('Test City')

		phone_custo = driver.find_element_by_name('customer_phone')
		phone_custo.send_keys('123456789')


		#seleccion pago paypal
		driver.find_element_by_xpath("//li[@data-pm='paypal']").click()


		#click boton continuar paypal
		time.sleep(1)
		driver.find_element_by_xpath(".//*[@class='button button-orange button-payment']").click()

		time.sleep(15)

		self.assertIn("PayPal", driver.title)


	def tearDown(self):

		self.driver.quit()


if __name__ == "__main__":
	unittest.main()

