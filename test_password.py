import unittest
import time
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
from datetime import datetime,timedelta
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.chrome.options import Options

class PythonOrgSearch(unittest.TestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument("--start-maximized")

		#self.driver = webdriver.Chrome('C:/chromedriver_win32/chromedriver.exe',options=chrome_options)
		self.driver = webdriver.Remote(command_executor='http://jenkins.floraqueen.net:4444/wd/hub',options=chrome_options,desired_capabilities=DesiredCapabilities.CHROME)

	def test_search_in_python_org(self):
		driver = self.driver
		driver.get('https://flowers-stage-en.floraqueen.net');

		time.sleep(2)

		try:
			driver.find_element_by_css_selector("button.close").click()
		except Exception as e:
			pass	


		menu = driver.find_element_by_css_selector("i.fq-user_full")
		actions = ActionChains(driver)
		actions.move_to_element(menu)
		actions.perform()


		time.sleep(1)
		driver.find_element_by_xpath(".//a[@id='bt-forgot-password']").click()

		time.sleep(1)
		email= driver.find_element_by_name('email')
		email.send_keys('test.floraqueen.test@gmail.com')
		driver.find_element_by_xpath(".//*[@class='button button-orange button-continue']").click()

		time.sleep(1)
		driver.get('https://accounts.google.com/signin/v2/identifier?service=mail&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&hl=es&flowName=GlifWebSignIn&flowEntry=ServiceLogin#__utma=29003808.18383240.1541166934.1541166934.1541166934.1&__utmb=29003808.0.10.1541166934&__utmc=29003808&__utmx=-&__utmz=29003808.1541166934.1.1.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided)&__utmv=-&__utmk=56058088');


		gmail= driver.find_element_by_name('identifier')
		gmail.send_keys('test.floraqueen.test@gmail.com')
		driver.find_element_by_xpath(".//*[@class='CwaK9']").click()

		time.sleep(4)
		gmail= driver.find_element_by_name('password')
		gmail.send_keys('F!oraqueen1990')
		time.sleep(8)
		driver.find_element_by_xpath(".//*[@class='CwaK9']").click()

		time.sleep(4)
		driver.find_element_by_xpath(".//div[@class='yW']").click()

		time.sleep(2)
		#element=driver.find_element_by_class_name("m_-5691234375272977278parasec").text()
		# element=driver.find_element_by_xpath("//td[@class='m_-5691234375272977278parasec']").text
		# print(element)

		elem = driver.find_element_by_xpath("//a[contains(text(),'Log in to FloraPrime')]").click()

		time.sleep(10)

		driver.switch_to.window(driver.window_handles[1])

		header = driver.find_element_by_css_selector('header.l-header') 
		assert "Hi Test!" in header.text
		time.sleep(10)

	def tearDown(self):

		self.driver.quit()

if __name__ == "__main__":

	unittest.main()




